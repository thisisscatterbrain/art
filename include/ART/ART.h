// ART - Augmented Reality Tracking.
//
// Wraps ARToolkit 2.7 to provide a simple maker-based camera tracking interface.
#ifndef ART_H
#define ART_H

#include <stdbool.h>
#include <stdint.h>

#define ART_MAX_PATTERN_COUNT 8

#ifdef __cplusplus
extern "C" {
#endif

void	art_ConfigSetCameraFile(const char* filename);
void	art_ConfigSetResolution(int32_t width, int32_t height);

void	art_Init();
void	art_Deinit();

// Loads an AR pattern from disk. width is the width of the printed pattern in
// millimeters. Returns -1 of loading the pattern failed, or otherwise a pattern
// handle.
int32_t	art_LoadPattern(const char* filename, double width);
void	art_Detect(uint8_t* frame);
bool	art_GetView(const float** view, int32_t patternId);

#ifdef __cplusplus
}
#endif

#endif