# ART - Augmented Reality Camera Tracking #
Wrap around ARToolkit 2.7 to provide a simple interface solely for camera tracking.

## Some Notes ##
### Building mk\_patt ###
- Architecture needs to be 32bit. As the Quicktime API has been removed for 64-bit architectures (see http://stackoverflow.com/questions/6679466/sequence-grabber).

## Todo's ##
- Currently only tested on a singe iOS platform.
