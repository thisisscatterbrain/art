#include "ART.h"
#include <assert.h>
#include <string.h>
#include <AR/ar.h>
#include <AR/gsub.h>

static struct {
	char		filename[1024];
	int32_t		width;
	int32_t		height;
	double		cameraNear;
	double		cameraFar;
} config = {"", 640, 480, 0.01, 1000.0};

static struct {
	ARParam		cameraParameters;
	float		projectionMatrix[16];
	bool		isInitialized;
	ARMarkerInfo*	markerInfo;
} state;

static struct {
	bool		isActive;
	double		width;
	int32_t		infoId;
	float		view[16];
} pattern[ART_MAX_PATTERN_COUNT];

// Computes the projection matrix from the ar camera parameters and stores them.
static void SetProjectionMatrix()
{
	ARParam acp;
	acp = state.cameraParameters;
	
	for (int i = 0; i < 4; i++) {
		acp.mat[1][i] = (state.cameraParameters.ysize - 1)*
			(state.cameraParameters.mat[2][i]) -
			state.cameraParameters.mat[1][i];
	}
	
	double pm[16];
	argConvGLcpara(&acp, config.cameraNear, config.cameraFar, pm);

	for (int i = 0; i < 32; i++)
		state.projectionMatrix[i] = (float)pm[i];
}

void art_ConfigSetCameraFile(const char* filename)
{
	assert(!state.isInitialized);
	assert(filename);
	strncpy(config.filename, filename, sizeof(config.filename));
}

void art_ConfigSetResolution(int32_t width, int32_t height)
{
	assert(!state.isInitialized);
	assert((width > 0) && (height > 0));
	config.width = width;
	config.height = height;
}

void art_Init()
{
	assert(!state.isInitialized);
	int err = arParamLoad(config.filename, 1, &state.cameraParameters);
	assert(err == 0);
	arParamChangeSize(&state.cameraParameters, config.width, config.height,
		&state.cameraParameters);
	arInitCparam(&state.cameraParameters);
	SetProjectionMatrix();
}

void art_Deinit()
{
}

int32_t art_LoadPattern(const char* filename, double width)
{
	assert(state.isInitialized);
	assert(filename && (width > 0.0));
	int id = arLoadPatt(filename);
	
	if (id < 0 || id >= ART_MAX_PATTERN_COUNT)
		return -1;
	
	pattern[id].isActive = true;
	pattern[id].infoId = -1;
	pattern[id].width = width;
	return id;
}

void art_Detect(uint8_t* frame)
{
	assert(state.isInitialized);
	int nmarkers;

	if (arDetectMarker(frame, 100, &state.markerInfo, &nmarkers) < 0)
		return;
	
	for (int i = 0; i < nmarkers; i++) {
		int idx = state.markerInfo[i].id;

		if (idx >= ART_MAX_PATTERN_COUNT || idx < 0)
			continue;
		
		if (pattern[idx].isActive)
		
			// If the pattern wasn't detected before or the same
			// pattern was detected with higher confidence, save i
			// as the index to the marker info array.
			if ((pattern[idx].infoId < 0)
			|| (state.markerInfo[pattern[idx].infoId].cf <
				state.markerInfo[i].cf))
				pattern[idx].infoId = i;
	}
}

bool art_GetView(const float** view, int32_t patternId)
{
	assert(patternId >= 0 && (patternId < ART_MAX_PATTERN_COUNT));
	if (pattern[patternId].isActive && (pattern[patternId].infoId >= 0))
		return true;
	else
		return false;
}





























